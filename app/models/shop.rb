class Shop < ActiveRecord::Base
  include PgSearch
  pg_search_scope :search,
  				  :against => [:name, :city],
  				  :using => {
  				  	:tsearch => {:dictionary => "english"}
  				  }  				  

  geocoded_by :address
  after_validation :geocode, :if => :address_changed?
end
