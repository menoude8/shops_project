namespace :import do

  desc "Import shops from csv"
  task shops: :environment do
    filename = File.join Rails.root, "lib/assets/shopmium.csv"
    counter = 0

    CSV.foreach(filename, headers: true) do |row|
	  p row
	  p row["name"]  
	  shop = Shop.create!(chain: row[0], name: row[1], latitude: row[2], longitude: row[3], address: row[4], city: row[5], zip: row[6], phone: row[8], country_code: row[18])
	  puts "#{name} - #{shop.errors.full_message.join(",")}" if shop.errors.any?
	  counter += 1 if shop.persisted?
	end

    puts "Imported #{counter} shops"
  end
end

#rake import:shops